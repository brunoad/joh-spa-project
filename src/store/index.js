import Vue from 'vue'
import Vuex from 'vuex'

import produto from './produto'
import adicional from './adicional'
import cliente from './cliente'
import fornecedor from './fornecedor'
import caixa from './caixa'
import motoboy from './motoboy'
import venda from './venda'
import usuario from './usuario'
import balanco from './balanco'
import pagamentoTipo from './pagamento-tipo'

Vue.use(Vuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation
 */

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      produto,
      adicional,
      cliente,
      fornecedor,
      caixa,
      motoboy,
      venda,
      usuario,
      balanco,
      'pagamento-tipo': pagamentoTipo
    }
  })

  return Store
}
