const defaultData = () => ({
  tipo: 0
})

export default {
  'SET_LIST': (state, data) => {
    state.list = data
  },
  'SET_ONE': (state, data) => {
    data = Object.assign(defaultData(), data)
    state.one = data
  }
}
