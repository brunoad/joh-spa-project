import { produto } from 'src/services'
const defaultFetchList = async (params) => {
  const data = (await produto.find({params}))
  return data
}

const defaultFetchOne = async (id) => {
  const data = (await produto.findById(id)).data
  return data
}

const saveDefault = async (data = {}) => {
  data.itens_produto = data.itens_produto.some(res => res.hasOwnProperty('id')) ? data.itens_produto : []
  return produto.save(data)
}

export default {
  /* SAVE */
  'SAVE': (store, data) => saveDefault(data),
  'SAVE_LANCHE': (store, data) => saveDefault(Object.assign(data, {produto_tipo_id: 1})),
  'SAVE_BEBIDA': (store, data) => saveDefault(Object.assign(data, {produto_tipo_id: 2})),
  'SAVE_DOCE': (store, data) => saveDefault(Object.assign(data, {produto_tipo_id: 3})),

  /* Fetch One */
  'FETCH_ONE': async ({ commit }, id) => {
    let data
    if (id) data = (await defaultFetchOne(id)).data
    commit('SET_ONE', data)
  },
  /* Fetch List */
  'FETCH_LIST': async ({ commit }, params) => {
    const data = (await defaultFetchList(params))
    commit('SET_LIST', data)
  },
  'FETCH_LIST_LANCHE': async ({ commit }, params) => {
    const data = (await defaultFetchList(Object.assign({ produto_tipo_id: 1 }, params)))
    commit('SET_LIST', data)
  },
  'FETCH_LIST_BEBIDA': async ({ commit }, params) => {
    const data = (await defaultFetchList(Object.assign({ produto_tipo_id: 2 }, params)))
    commit('SET_LIST', data)
  },
  'FETCH_LIST_DOCE': async ({ commit }, params) => {
    const data = (await defaultFetchList(Object.assign({ produto_tipo_id: 3 }, params)))
    commit('SET_LIST', data)
  },

  /* Remove */
  'REMOVE': ({ commit }, id) => produto.remove(id)
}
