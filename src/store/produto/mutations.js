const defaultData = () => ({
  descricao: '',
  url_image: '',
  valor_venda: 0,
  valor_custo: 0,
  itens_produto: []
})

export default {
  'SET_LIST': (state, data) => {
    state.list = data
  },
  'SET_ONE': (state, data) => {
    data = Object.assign(defaultData(), data)
    state.one = data
  }
}
