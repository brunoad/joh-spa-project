import { cliente } from 'src/services'
const defaultFetchList = async (params) => {
  const data = (await cliente.find({params}))
  return data
}

const defaultFetchOne = async (id) => {
  const data = (await cliente.findById(id)).data
  return data
}

export default {
  /* SAVE */
  'SAVE': (store, data) => cliente.save(data),
  /* Fetch One */
  'FETCH_ONE': async ({ commit }, id) => {
    let data
    if (id) data = (await defaultFetchOne(id)).data
    commit('SET_ONE', data)
  },
  /* Fetch List */
  'FETCH_LIST': async ({ commit }, params) => {
    const data = (await defaultFetchList(params))
    commit('SET_LIST', data)
  },
  /* Remove */
  'REMOVE': ({ commit }, id) => cliente.remove(id)
}
