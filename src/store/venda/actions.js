import { venda } from 'src/services'
const defaultFetchList = async (params) => {
  const data = (await venda.find({params}))
  return data
}

const defaultFetchOne = async (id) => {
  const data = (await venda.findById(id)).data
  return data
}

export default {
  /* SAVE */
  'SAVE': (store, data) => venda.save(data),
  /* Fetch One */
  'FETCH_ONE': async ({ commit }, id) => {
    let data
    if (id) data = (await defaultFetchOne(id)).data
    commit('SET_ONE', data)
  },
  /* Fetch List */
  'FETCH_LIST': async ({ commit }, params) => {
    const data = (await defaultFetchList(params))
    commit('SET_LIST', data)
  },
  /* Remove */
  'REMOVE': ({ commit }, id) => venda.remove(id),
  // =========================================== //
  // =========================================== //
  // ===============  Vendas  ================== //
  // =========================================== //
  // =========================================== //
  'SAVE_VENDA': ({ state }) => {
    const data = Object.assign(state.venda, { produtos: state.produto_venda })
    console.log('Venda: ', data)
    return venda.save(data)
  }
}
