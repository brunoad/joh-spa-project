/* eslint-disable */

const defaultData = () => ({
  descricao: '',
  url_image: '',
  valor_venda: 0,
  valor_custo: 0,
  itens_venda: []
})

const vendaDefault = () => ({
  motoboy_id: null,
  venda_id: null,
  pagamento_tipo: 0,
})

const produtoVenda = () => ([])
const venda = () => ({})

export default {
  'SET_LIST': (state, data) => {
    state.list = data
  },
  'SET_ONE': (state, data) => {
    data = Object.assign(defaultData(), data)
    state.one = data
  },
  'CLEAR': (state) => {
    state.produto_venda = produtoVenda()
    state.venda = venda()
  },
  'SET_VENDA': (state, data) => {
    data = Object.assign(vendaDefault(), data)
    state.venda = data
  },
  'ADD_PRODUTO': (state, data) => {
    data.key = new Date().getTime()
    data.quantidade = 1
    data.adicionais = data.adicionais || []
    state.produto_venda.push(Object.assign({}, data))
  },
  'REMOVE_PRODUTO': (state, key) => {
    state.produto_venda = state.produto_venda.filter(res => res.key !== key)
  },
  'ADD_ADICIONAL': (state, { produto_key, data }) => {
    data.key = new Date().getTime()
    data.quantidade = 1
    state.produto_venda.map(o => {
      if (o.key === produto_key) {
        o.adicionais.push(data)
      }
      return o
    })
  },
  'REMOVE_ADICIONAL': (state, adicional_key) => {
    state.produto_venda = state.produto_venda.map(o => {
      o.adicionais = o.adicionais.filter(res => res.key !== adicional_key)
      return o
    })
  },
  'SET_TIPO_PAGAMENTO': (state, data) => {
    state.venda.pagamento_tipo = data
  }
}
