export default {
  'LIST': (state) => state.list,
  'ONE': (state) => state.one,
  'VENDA': (state) => state.venda,
  'PRODUTO_VENDA': (state) => state.produto_venda
}
