const config = {
  route_name: 'caixa',
  route_path: 'caixas',
  page_title: {
    list: 'Lista de Caixas',
    create: 'Inserir novo Caixas',
    edit: 'Editar Caixas'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'CAIXA')
