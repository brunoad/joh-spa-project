const config = {
  route_name: 'adicional',
  route_path: 'adicionais',
  page_title: {
    list: 'Lista de Adicionais',
    create: 'Inserir novo Adicional',
    edit: 'Editar Adicional'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'ADICIONAL')
