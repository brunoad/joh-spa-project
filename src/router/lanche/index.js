const config = {
  route_name: 'lanche',
  route_path: 'lanches',
  product_type: 3,
  page_title: {
    list: 'Lista de Lanches',
    create: 'Inserir novo Lanche',
    edit: 'Editar Lanche'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'LANCHE')
