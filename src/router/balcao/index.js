const config = {
  route_name: 'atendimento-balcao',
  route_path: 'atendimento-balcao'
}
const recursoKey = ''
export default [
  {
    name: `${config.route_name}`,
    path: `/${config.route_path}`,
    meta: {
      title: 'Atendimento balcão',
      recursoKey
    },
    component: () => import(`pages/balcao`)
  },
  {
    name: '',
    path: `/${config.route_path}/pedido`,
    component: () => import(`pages/balcao/pedido`),
    meta: {
      title: 'Novo pedido',
      recursoKey
    },
    children: [
      {
        name: 'pedido-menu',
        path: ``,
        component: () => import(`pages/balcao/pedido/menu`),
        meta: {
          title: 'Monte seu Pedido',
          recursoKey
        }
      },
      {
        name: 'pedido-tabela',
        path: `tabela`,
        component: () => import(`pages/balcao/pedido/table`),
        meta: {
          title: 'Novo pedido',
          recursoKey
        }
      }
    ]
  },
  {
    name: `finalizar-pedido`,
    path: `/${config.route_path}/finalizar-pedido`,
    meta: {
      title: 'Resumo Pedido balcão',
      recursoKey
    },
    component: () => import(`pages/balcao/pedido/finalizar-pedido`)
  }
]
