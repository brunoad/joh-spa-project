const config = {
  route_name: 'balanco',
  route_path: 'balancos',
  page_title: {
    list: 'Balanços',
    create: 'Inserir novo Balanço',
    edit: 'Editar Balanço'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'CAIXA_INFO')
