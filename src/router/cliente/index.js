const config = {
  route_name: 'cliente',
  route_path: 'clientes',
  page_title: {
    list: 'Lista de Clientes',
    create: 'Inserir novo Cliente',
    edit: 'Editar Cliente'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'CLIENTE')
