const config = {
  route_name: 'pagamento-tipo',
  route_path: 'pagamento-tipos',
  page_title: {
    list: 'Lista de Tipos de Pagamento',
    create: 'Inserir novo Tipo de Pagamento',
    edit: 'Editar Tipo de Pagamento'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'MEIO_PAGAMENTO')
