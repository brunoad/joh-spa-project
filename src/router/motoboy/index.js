const config = {
  route_name: 'motoboy',
  route_path: 'motoboys',
  page_title: {
    list: 'Lista de Motoboy',
    create: 'Inserir novo Motoboy',
    edit: 'Editar Motoboy'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'MOTOBOY')
