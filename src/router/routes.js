import storage from 'src/plugins/storage_helpers'
import bebida from './bebida'
import lanche from './lanche'
import doce from './doce'
import cliente from './cliente'
import adicional from './adicional'
import fornecedor from './fornecedor'
import caixa from './caixa'
import balcao from './balcao'
import motoboy from './motoboy'
import cadastro from './cadastro'
import usuario from './usuario'
import venda from './venda'
import balanco from './balanco'
import pagamentoTipo from './pagamento-tipo'

const routes = [
  {
    path: '/app',
    component: () => import('layouts/LayoutApp.vue'),
    children: [
      {
        path: '',
        meta: {
          title: 'Bem Vindo'
        },
        component: () => import('pages/Index.vue')
      },
      ...bebida,
      ...lanche,
      ...doce,
      ...cliente,
      ...adicional,
      ...fornecedor,
      ...caixa,
      ...balcao,
      ...motoboy,
      ...cadastro,
      ...usuario,
      ...venda,
      ...balanco,
      ...pagamentoTipo
    ],
    beforeEnter: (to, from, next) => {
      if (storage.getToken()) next()
      else next('/')
    }
  },
  {
    path: '/',
    component: () => import('layouts/Login.vue'),
    beforeEnter: (to, from, next) => {
      if (!storage.getToken()) next()
      else next('/app')
    }
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
