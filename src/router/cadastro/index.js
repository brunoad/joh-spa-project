const config = {
  route_name: 'cadastro',
  route_path: 'cadastros'
}

export default [
  {
    name: `${config.route_name}`,
    path: `/${config.route_path}`,
    meta: {
      title: 'Cadastros do Sistema'
    },
    component: () => import(`pages/cadastro`)
  }
]
