const buildRoute = (config, pathCrud, recursoKey = '') => {
  return [
    {
      name: `${config.route_name}-listagem`,
      path: `/${config.route_path}`,
      meta: {
        recursoKey,
        pathCrud,
        title: config.page_title.list
      },
      component: () => import(`pages/${config.route_name}/listagem.vue`)
    },
    {
      name: `${config.route_name}-novo`,
      path: `/${config.route_path}/novo`,
      meta: {
        recursoKey,
        pathCrud,
        title: config.page_title.create
      },
      component: () => import(`pages/${config.route_name}/formulario.vue`)
    },
    {
      name: `${config.route_name}-editar`,
      path: `/${config.route_path}/:id`,
      meta: {
        recursoKey,
        pathCrud,
        title: config.page_title.edit
      },
      component: () => import(`pages/${config.route_name}/formulario.vue`)
    }
  ]
}

export { buildRoute }
