const config = {
  route_name: 'venda',
  route_path: 'vendas',
  page_title: {
    list: 'Lista de Clientes',
    create: 'Inserir novo Cliente',
    edit: 'Editar Cliente'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

export default [
  {
    name: `${config.route_name}-novo-listagem`,
    path: `/${config.route_path}-novo`,
    meta: {
      pathCrud,
      title: config.page_title.list,
      recursoKey: 'PEDIDO_NOVO'
    },
    component: () => import(`pages/${config.route_name}/listagem.vue`)
  },
  {
    name: `${config.route_name}-entrega-listagem`,
    path: `/${config.route_path}-espera`,
    meta: {
      pathCrud,
      title: config.page_title.list,
      recursoKey: 'PEDIDO_ENTREGA'
    },
    component: () => import(`pages/${config.route_name}/listagem.vue`)
  },
  {
    name: `${config.route_name}-espera-listagem`,
    path: `/${config.route_path}-espera`,
    meta: {
      pathCrud,
      title: config.page_title.list,
      recursoKey: 'PEDIDO_ESPERA'
    },
    component: () => import(`pages/${config.route_name}/listagem.vue`)
  },
  {
    name: `${config.route_name}-finalizado-listagem`,
    path: `/${config.route_path}-finalizado`,
    meta: {
      pathCrud,
      title: config.page_title.list,
      recursoKey: 'PEDIDO_FINALIZADO'
    },
    component: () => import(`pages/${config.route_name}/listagem.vue`)
  },
  {
    name: `${config.route_name}-novo`,
    path: `/${config.route_path}/novo`,
    meta: {
      pathCrud,
      title: config.page_title.create
    },
    component: () => import(`pages/${config.route_name}/formulario.vue`)
  },
  {
    name: `${config.route_name}-editar`,
    path: `/${config.route_path}/:id`,
    meta: {
      pathCrud,
      title: config.page_title.edit
    },
    component: () => import(`pages/${config.route_name}/formulario.vue`)
  }
]
