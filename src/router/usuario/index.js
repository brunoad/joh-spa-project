const config = {
  route_name: 'usuario',
  route_path: 'usuarios',
  page_title: {
    list: 'Lista de Usuários',
    create: 'Inserir novo Usuários',
    edit: 'Editar Usuários'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'USUARIO')
