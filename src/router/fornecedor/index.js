const config = {
  route_name: 'fornecedor',
  route_path: 'fornecedores',
  page_title: {
    list: 'Lista de Fornecedores',
    create: 'Inserir novo Fornecedores',
    edit: 'Editar Fornecedores'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'FORNECEDOR')
