const config = {
  route_name: 'bebida',
  route_path: 'bebidas',
  product_type: 2,
  page_title: {
    list: 'Lista de Bebidas',
    create: 'Inserir nova Bebida',
    edit: 'Editar Bebida'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'BEBIDA')
