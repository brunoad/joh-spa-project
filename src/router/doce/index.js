const config = {
  route_name: 'doce',
  route_path: 'doces',
  product_type: 3,
  page_title: {
    list: 'Lista de Doces/Sobremesas',
    create: 'Inserir novo Doce/Sobremesa',
    edit: 'Editar Doce/Sobremesa'
  }
}

const pathCrud = {
  novo: `${config.route_name}-novo`,
  editar: `${config.route_name}-editar`,
  listagem: `${config.route_name}-listagem`
}

import { buildRoute } from '../buildRoute'
export default buildRoute(config, pathCrud, 'DOCE')
