import http from 'src/services/http'

export default ({ Vue }) => {
  Vue.prototype.$http = http
}
