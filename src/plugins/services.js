import * as services from 'src/services'
import { permissao } from 'src/plugins/storage_helpers'

export default ({ Vue }) => {
  Vue.prototype.$services = services
  Vue.prototype.$permissao = permissao
}
