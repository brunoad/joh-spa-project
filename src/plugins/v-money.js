import money from 'v-money'
export default ({ Vue }) => {
  Vue.prototype.$percentFormat = {
    decimal: ',',
    thousands: '.',
    prefix: '% ',
    suffix: '',
    precision: 2,
    masked: false
  }
  Vue.use(money, {
    decimal: ',',
    thousands: '.',
    prefix: 'R$ ',
    suffix: '',
    precision: 2,
    masked: false
  })
}
