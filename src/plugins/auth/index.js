import { register, login, logout } from 'src/plugins/storage_helpers'

export default ({ Vue }) => {
  Vue.prototype.$auth = {
    register,
    login,
    logout
  }
}
