import axios from 'axios'
let http = axios.create({
  baseURL: process.env.API
})

const storage = {
  // ======== Token ======== //
  setToken: (token) => localStorage.setItem('joh_app:token', token),
  getToken: () => localStorage.getItem('joh_app:token'),
  setRefreshToken: (refreshToken) => localStorage.setItem('joh_app:refreshToken', refreshToken),
  getRefreshToken: () => localStorage.getItem('joh_app:refreshToken'),
  deleteToken: () => {
    localStorage.removeItem('joh_app:token')
    location.reload()
  },
  // ======== Usuario ======== //
  setUser: (token) => localStorage.setItem('joh_app:token', token),
  getUser: () => localStorage.getItem('joh_app:user'),
  // ========================= //
  // ========================= //

  // ======== Caixa ======== //
  setCaixaId: (token) => localStorage.setItem('joh_app:caixa_id', token),
  getCaixaId: () => localStorage.getItem('joh_app:caixa_id'),
  setCaixaStatus: (token) => localStorage.setItem('joh_app:caixa_status', token),
  getCaixaStatus: () => localStorage.getItem('joh_app:caixa_status'),
  // Permissão
  getPermissao: () => {
    let perm = localStorage.getItem('joh_app:permissao')
    return perm ? perm.split(',') : []
  },
  setPermissao: (permissao) => localStorage.setItem('joh_app:permissao', permissao)
}

const register = async ({email, username, password}) => http.post('register', {email, username, password})

const authentication = async ({email, password}) => http.post('authenticate', {email, password}, {email, password})

const refreshToken = async (rtoken) => {
  const { token, refreshToken } = (await http.post('refres-token', {refresh_token: rtoken})).data
  storage.setToken(token)
  storage.setRefreshToken(refreshToken)
}

const login = async ({email, password}, refresh) => {
  const { token, permissoes } = (await authentication({email, password})).data
  storage.setToken(token)
  storage.setPermissao(permissoes)
  location.reload()
}

const getCaixa = () => {
  return {
    caixa_id: storage.getCaixaId(),
    status: storage.getCaixaStatus()
  }
}
const setCaixa = ({caixa_id, status}) => { // eslint-disable-line
  storage.setCaixaId(caixa_id) // eslint-disable-line
  storage.setCaixaStatus(status)
  location.reload()
}

const logout = storage.deleteToken
const permissao = {
  get: () => storage.getPermissao(),
  has: (permissao) => storage.getPermissao().some(res => res === permissao) || !permissao
}
export default storage
export {
  register,
  login,
  logout,
  refreshToken,
  getCaixa,
  setCaixa,
  permissao
}
