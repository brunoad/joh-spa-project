import axios from 'axios'
import store, { refreshToken } from 'src/plugins/storage_helpers'
import { Notify, Loading } from 'quasar'

const http = axios.create({
  baseURL: process.env.API
})

http.interceptors.request.use(function (config) {
  Loading.show()
  // Do something before request is sent
  config.headers.Authorization = `Bearer ${store.getToken()}`
  return config
}, function (error) {
  // Do something with request error
  return Promise.reject(error)
})

// Add a response interceptor
http.interceptors.response.use(function (response) {
  Loading.hide()
  if (response.data.message) {
    Notify.create({
      color: 'positive',
      message: response.data.message,
      icon: 'check',
      position: 'top-right'
    })
  }
  // Do something with response data
  return response
}, function (error) {
  Loading.hide()
  if (Array.isArray(error.response.data)) {
    error.response.data.message = error.response.data[0].message || 'Erro desconhecido'
  }
  if (error.response.data.message) {
    Notify.create({
      color: 'negative',
      message: error.response.data.message,
      icon: 'info',
      position: 'top-right'
    })
  }
  if (error.response.status === 401) {
    const rToken = store.getRefreshToken()
    if (!rToken) store.deleteToken()
    else {
      refreshToken(rToken)
    }
  }
  // Do something with response error
  return Promise.reject(error)
})

export default http
