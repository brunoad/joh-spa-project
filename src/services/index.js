import { buildService } from './services_build'
import http from './http'
import { setCaixa } from 'src/plugins/storage_helpers'

const produto = buildService('produtos')
const produtoImagem = buildService('produto-imagems')
const adicional = buildService('adicionais')
const cliente = buildService('clientes')
const fornecedor = buildService('fornecedores')
const caixa = buildService('caixas')
const motoboy = buildService('motoboys')
const venda = buildService('vendas')
const usuario = buildService('users')
const balanco = buildService('balancos')
const pagamentoTipo = buildService('pagamento-tipos')
const recurso = buildService('recursos')

const abrirCaixa = async (id) => {
  const data = await http.post(`caixas/${id}/abrir`)
  setCaixa(data.data.data)
}

const fecharCaixa = async (id) => {
  const data = await http.post(`caixas/${id}/fechar`)
  setCaixa(data.data.data)
}

const getStatusCaixa = (id) => {
  return http.get(`status-caixa`)
}

const mudarStatusPedido = async ({id, status}) => {
  return http.put(`vendas/${id}/mudar-status`, { status })
}

export {
  abrirCaixa,
  fecharCaixa,
  produto,
  produtoImagem,
  adicional,
  cliente,
  fornecedor,
  caixa,
  motoboy,
  venda,
  usuario,
  balanco,
  mudarStatusPedido,
  getStatusCaixa,
  pagamentoTipo,
  recurso
}
