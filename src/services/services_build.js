import http from './http'

const buildService = (route) => ({
  findById: (id) => http.get(`${route}/${id}`),
  find: (config) => {
    return http.get(`${route}`, config)
  },
  save: (data) => data.id ? http.put(`${route}/${data.id}`, data) : http.post(`${route}`, data),
  remove: (id) => http.delete(`${route}/${id}`)
})

export {
  buildService
}
